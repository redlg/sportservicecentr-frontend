jQuery(document).ready(function($) {
    if(!Page.is('/'))
        return;

    $(".index-promo-slider__wrap").owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        navText: [
            '<i class="mdi mdi-chevron-left"></i>',
            '<i class="mdi mdi-chevron-right"></i>'
        ]
    });

    $(".news-slider__slider").owlCarousel({
        responsive: {
            0: {
                items: 1,
            },
            540: {
                items: 2,
            },
            980: {
                items: 4,
            }
        },
        loop: false,
        nav: true,
        dots: false,
        margin: 20,
        navText: [
            '<i class="mdi mdi-chevron-left"></i>',
            '<i class="mdi mdi-chevron-right"></i>'
        ]
    });

    $(".brands__slider").owlCarousel({
        responsive: {
            0: {
                items: 2,
            },
            540: {
                items: 3,
            },
            980: {
                items: 4,
            },
            1280: {
                items: 6,
            },
            1578: {
                items: 8
            }
        },
        loop: true,
        nav: true,
        dots: false,
        margin: 73,
        navText: [
            '<i class="mdi mdi-chevron-left"></i>',
            '<i class="mdi mdi-chevron-right"></i>'
        ]
    });

    $(".index-section").height($('.index-section--1').width());
    $(window).resize(function(event) {
        $(".index-section").height($('.index-section--1').width());
    });

    // $(".catalog-item-preview").height($('.catalog-item-preview--1').width());
    // $(window).resize(function(event) {
    //     $(".catalog-item-preview").height($('.catalog-item-preview--1').width());
    // });
});
