$(document).ready(function() {
    $(".catalog-item__property").click(function(event) {
        if($(this).hasClass('catalog-item__property--disabled'))
            return;
        $(this).parent().find('.catalog-item__property').removeClass('catalog-item__property--active');
        $(this).addClass('catalog-item__property--active');
    });
});
