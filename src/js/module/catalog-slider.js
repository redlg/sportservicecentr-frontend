jQuery(document).ready(function($) {
    $(".catalog-slider .owl-carousel").owlCarousel({
        responsive: {
            0: {
                items: 1,
            },
            540: {
                items: 2,
            },
            980: {
                items: 3,
            },
            1280: {
                items: 4,
            }
        },
        loop: true,
        nav: true,
        dots: true,
        margin: 0,
        navText: [
            '<i class="mdi mdi-chevron-left"></i>',
            '<i class="mdi mdi-chevron-right"></i>'
        ]
    });
});
