var Mountains = {
    active: false,
    init: function(){
        Mountains.run();
        $(window).scroll(function(){
            Mountains.run();
        });
    },
    run: function(){
        if(!this.active){
            this.active = false;
            var calc = $(window).scrollTop() - $('.mountains').offset().top;
            if(calc > -($(window).height()*0.6)){
                $('.mountains').addClass('mountains--active');
                setTimeout(function(){
                    $('.mountains__slide').addClass('mountains__slide--active');
                }, 2000);
            }
        }
    }
}
