var HeightCalc = {
    run: function(){
        var width = 0,
            additional = 0,
            multiplier = 1;
        $(".height-calc").each(function(index, el) {
            if(multiplier = $(this).attr('multiplier') === undefined)
                multiplier = 1;

            multiplier = parseFloat(multiplier);

            if(additional = $(this).attr('additional') === undefined)
                additional = 0;

            width = $(this).width();
            $(this).height(width*multiplier + additional);
        });
    }
}
$(document).ready(function() {
    HeightCalc.run();
    $(window).resize(HeightCalc.run);
});
