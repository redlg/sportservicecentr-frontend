jQuery(document).ready(function($) {
    $(".catalog-detail-preview__image").click(function(event) {
        $(".catalog-detail-preview__image").removeClass('catalog-detail-preview__image--active');
        $(this).addClass('catalog-detail-preview__image--active');
        $(".catalog-detail-preview__photo img").attr('src', $(this).attr('src'));
    });
});
