jQuery(document).ready(function($) {
    $(".catalog-filter-block__title").click(function(event) {
        $(this).parents('.catalog-filter-block').toggleClass('catalog-filter-block--hidden');
    });
    $(".catalog-filter-range__slider").slider({
        range: true,
        min: 950,
        max: 30000,
        values: [ 950, 30000 ],
        slide: function( event, ui ) {
            $(this).parents('.catalog-filter-block').find('.catalog-filter-range__value--min').val(ui.values[ 0 ]);
            $(this).parents('.catalog-filter-block').find('.catalog-filter-range__value--max').val(ui.values[ 1 ]);
        }
    });
});
